Compilation guide.

Requirements: Visual Studio 2005 Service Pack 1.

Warning: As I am using Windows Spanish version, I don't have c:\Program Files, instead I have c:\Archivos de programa, so in Project Properties you have to correct this, go here:
Project Properties - Configuration Properties - C/C++ - General - Additional Include Directories
Project Properties - Configuration Properties - Linker - General - Additional Library Directories
And change it.


Compilation guide.

Download and install FCOLLADA 3.05B. Compile Release Unicode and Debug Unicode versions of FCOLLADA (do not use DLL, I don't like them).

Download wglext.h from oss.sgi.com (use google) and copy it to C:\Archivos de programa\Microsoft Visual Studio 8\VC\PlatformSDK\Include\gl

Download glext.h from oss.sgi.com (use google) and copy it to C:\Archivos de programa\Microsoft Visual Studio 8\VC\PlatformSDK\Include\gl

Copy DevIL.dll and ILU.dll from C:\Archivos de programa\Feeling Software\External\DevIL\lib\Win32 to c:\Windows\system32

Download and install GLEW 1.5.0 using their website guide.

Download and install GLUT 3.7.6 using its README file.

That's all folks.