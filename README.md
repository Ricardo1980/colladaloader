This project is about an open source application to load and visualize COLLADA files in real time using OpenGL.

I uploaded it originally to SourceForge in 2007-06-06.
https://sourceforge.net/projects/colladaloader/

I realized that SourceForge added a .ZIP file (with an .EXE inside) without telling me anything, called Battle4_5-22-07.zip.

For that reason I moved the project to BitBucket. I respected the same structure I used in 2007, which is not the best one.